using UnityEngine;
using System.Collections;

public class Practice : MonoBehaviour {
	private float mfwindSliderValue=10;
	private float mfPositionValue=10;
	private static int miWindEffect;
	private static float midistanceToincrease = 90;
	public static Practice instance;
	private BallHandler _ballHandler;
	private bool ispause;
	public Vector3 windBtnPos;
	public Vector3 distBtnPos;
	public enum WindType
	
	{
		
		right=0,
		left
	}	
	public WindType _windType;
	private InputManager _inputManager;
	private CameraMovement _cameraMovement;
	// Use this for initialization
	void Start () {
		ispause = true;
		instance=this;
		_inputManager=GameObject.Find("GameManager").GetComponent<InputManager>();
		_cameraMovement=GameObject.Find("Main Camera").GetComponent<CameraMovement>();
		_ballHandler=GameObject.Find ("GameManager").GetComponent<BallHandler>();
		miWindEffect = 0;
		midistanceToincrease = 90;
		windBtnPos = new Vector3(0.056f,0.225f,1.5f);
		distBtnPos = new Vector3(0.94f,0.225f,1.5f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public  int GetPracticeWind()
	{
		switch(_windType)
		{
			case WindType.right:
			{
				return(miWindEffect);	
			}
			case WindType.left:
			{
				return(-miWindEffect);
			}
		   	default:
				return(0);
		}
	}
	public float GetDistanceToincrease()
	{
		return(midistanceToincrease);
	}
	public void ChangePosInZone()
	{
		if(_inputManager.GetInputStatus()==InputManager.InputStatus.idle ||_inputManager.GetInputStatus()==InputManager.InputStatus.rotationInput)
		{
			GameObject ball= _inputManager.GetRefOfBall();
			Vector3  ballPos = ball.transform.position;
			ballPos.x=Random.Range(-24,20);
			ballPos.z=midistanceToincrease + Random.Range(-5,5);
			ball.transform.position=ballPos;
			_cameraMovement.ChangeCameraDirection(ball);
			_ballHandler.SetballPos(ballPos);
		}
	}
	public void ChangeWindType(WindType windtype)
	{
		switch(windtype)
		{
			case WindType.left:
			{
				_windType=WindType.left;
				break;
			}
			
			case WindType.right:
			{
				_windType=WindType.right;
				break;
			}
		}
	}
	
	public void SetPauseCondition(bool x)
	{
		ispause = x;
	}
	
	public void setWindEffect(int x)
	{
		miWindEffect = x;
	}
	public void Setdistance(int x)
	{
		
		midistanceToincrease = 90-x;
	}
	
	public int GetDistance()
	{
		return(115-(int)midistanceToincrease);
	}
	
	
}
