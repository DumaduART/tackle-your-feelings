using UnityEngine;
using System.Collections;

public class AddMoveMent : MonoBehaviour {
	
	public int repeatCycle = 1;
	public float _fSpeedX;
	
	public float _fSpeedY;
	
    float mfXoffset;
	
	float mfYoffset;
	bool isXmoveMent = true;
	bool isYmoveMent;
	float xTarget;
	float yTarget;
	public Material _mMyMaterial;
	// Use this for initialization
	void Start () {
	
		_mMyMaterial.mainTextureOffset = new Vector2(0,0);
		//StartCoroutine(XmoveMent(mfXoffset+repeatCycle));
		xTarget = 1;
		yTarget = .25f;
	}
	
	void Update()
	{
		if(isXmoveMent)
		{
			if(mfXoffset<=xTarget)
				mfXoffset += _fSpeedX*Time.deltaTime;
			else
			{
				//xTarget += 1;
				mfXoffset = 0;
				isXmoveMent = false;
				isYmoveMent = true;
				if(mfYoffset>=1)
				{
					mfYoffset = 0;
					yTarget = .25f;
				}
			}
		}
		
		if(isYmoveMent)
		{
			if(mfYoffset<=yTarget)
			mfYoffset += _fSpeedY;
			else
			{
				yTarget += .25f;
				isXmoveMent = true;
				isYmoveMent = false;
			}
		}
		
		_mMyMaterial.mainTextureOffset = new Vector2(mfXoffset,mfYoffset);
	}
	
	public IEnumerator XmoveMent(float target)
	{
		while(mfXoffset<target)
		{
			mfXoffset = Mathf.MoveTowards(mfXoffset,target,_fSpeedX);
			_mMyMaterial.mainTextureOffset = new Vector2(mfXoffset,mfYoffset);
			yield return new WaitForEndOfFrame() ;
		}
		
		StartCoroutine(YmoveMent(mfYoffset+.25f));
	}
	
	public IEnumerator YmoveMent(float target)
	{
		while(mfYoffset<target)
		{
			mfYoffset = Mathf.MoveTowards(mfYoffset,target,_fSpeedY);
			_mMyMaterial.mainTextureOffset = new Vector2(mfXoffset,mfYoffset);
			yield return new WaitForEndOfFrame() ;
		}
		StartCoroutine(XmoveMent(mfXoffset+repeatCycle));
	}
}
