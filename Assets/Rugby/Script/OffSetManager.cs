using UnityEngine;
using System.Collections;

public class OffSetManager : MonoBehaviour {
	
	
	public float _fSpeedX;
	
	public float _fSpeedY;
	
	float mfXoffset;
	
	float mfYoffset;
	
	public Material _mMyMaterial;
	// Use this for initialization
	void Start () {
	
		_mMyMaterial.mainTextureOffset = new Vector2(0,0);
		
	}
	
	// Update is called once per frame
	void Update () {
	
		mfXoffset += _fSpeedX;
		
		mfYoffset += _fSpeedY;
		 
		
		_mMyMaterial.mainTextureOffset = new Vector2(mfXoffset,mfYoffset);
	}
}
