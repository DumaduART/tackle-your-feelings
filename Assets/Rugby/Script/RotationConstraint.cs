using UnityEngine;
using System.Collections;

public class RotationConstraint : MonoBehaviour {

	// Use this for initialization
	public enum ConstraintAxis
	{
		X = 0,
		Y,
		Z
	}
	public ConstraintAxis axis;
	public  float min ;
	public float max;
	public float range;
	private Vector3 rotateAround;
	private Transform thisTransform;
	private Quaternion minQuaternion;
	private Quaternion maxQuaternion ;
	void Start () {
		thisTransform = transform;
		Quaternion axisRotation = Quaternion.identity;
		switch ( axis )
		{
		case ConstraintAxis.X:
			rotateAround = Vector3.right;
			axisRotation = Quaternion.AngleAxis( thisTransform.localRotation.eulerAngles.x, rotateAround );
			//hisTransform.localRotation.eulerAngles[axis];
			break;
			
		case ConstraintAxis.Y:
			rotateAround = Vector3.up;
			axisRotation = Quaternion.AngleAxis( thisTransform.localRotation.eulerAngles.y, rotateAround );
			break;
			
		case ConstraintAxis.Z:
			rotateAround = Vector3.forward;
			axisRotation = Quaternion.AngleAxis( thisTransform.localRotation.eulerAngles.z, rotateAround );
			break;
		}
		
		minQuaternion = axisRotation * Quaternion.AngleAxis( min, rotateAround );
		maxQuaternion = axisRotation * Quaternion.AngleAxis( max, rotateAround );
		range = max - min;
	}
	
	// Update is called once per frame
	void LateUpdate () {
		
		Quaternion localRotation = thisTransform.localRotation;
		Quaternion axisRotation = Quaternion.AngleAxis( localRotation.eulerAngles.y, rotateAround );
		float angleFromMin = Quaternion.Angle( axisRotation, minQuaternion );
		float angleFromMax = Quaternion.Angle( axisRotation, maxQuaternion );
		
		if ( angleFromMin <= range && angleFromMax <= range )
		return;
		
		else
		{		
		// Let's keep the current rotations around other axes and only
			// correct the axis that has fallen out of range.
			Vector3 euler = localRotation.eulerAngles;			
			if ( angleFromMin > angleFromMax )
				euler.y = maxQuaternion.eulerAngles.y;
			else
				euler.y = minQuaternion.eulerAngles.y;
	
			thisTransform.localEulerAngles = euler;		
		}
	
	}
}
