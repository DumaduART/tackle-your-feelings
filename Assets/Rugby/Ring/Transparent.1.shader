﻿Shader "Custom/Transparent" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Color ("Main Color", Color) = (1,1,1,1)
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha 
		
		Pass 
        {            
            SetTexture[_MainTex] 
            {
            	constantColor [_Color]
                Combine texture*constant,texture*constant
            }
        }
	} 
	FallBack "Diffuse"
}
