﻿using UnityEngine;
using System.Collections;

public class FlashingCamera : MonoBehaviour {

	// Use this for initialization
	
	public GameObject[] cameraObj;
	public float _WatingTime;
	float startTime;
	int currentCam;
	int prevCam;
	void Start () {
		startTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
		if(Time.time-startTime >= _WatingTime)
		{
			prevCam = 	currentCam;			
			currentCam = Random.Range(0,cameraObj.Length);
			cameraObj[currentCam].SetActive(true);
			cameraObj[prevCam].SetActive(false);
			startTime = Time.time; 
		}
		
	}
	
	
}
