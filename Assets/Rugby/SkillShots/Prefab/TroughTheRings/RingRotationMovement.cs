using UnityEngine;
using System.Collections;

public class RingRotationMovement : MonoBehaviour {
	 public enum MovementType
	 {
		right=0,left
	 };
      public bool _isRotation;
	  public bool _ismovement;
	 // p GameObject _ring;
	  public MovementType movementType;
	  public bool isrestriction;
	  public float _rightRestriction = -3.0f;
	  public float _leftRestriction = -5.0f;
		
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
		if(_isRotation)
		{
			transform.Rotate(new Vector3(0,1,1) * Time.deltaTime*40);
		}
		if(_ismovement)
		{
			switch(movementType)
			{
				case MovementType.right:
				{
					transform.Translate(-Vector3.up * Time.deltaTime*2);
					
					break;
				}
				case MovementType.left:
				{
					transform.Translate(Vector3.up * Time.deltaTime*2);
					break;
				}
			}
		
		}
		if(transform.position.x>=_rightRestriction)
			movementType = MovementType.left;
		if(transform.position.x<=_leftRestriction)
			movementType = MovementType.right;
		if(isrestriction)
		{
			
			if(transform.localPosition.y>4)
			{
				Vector3 temp=transform.localPosition;
				temp.y = 4;
				transform.localPosition=temp;
			}
			if(transform.localPosition.y<-5)
			{
				Vector3 temp=transform.localPosition;
				temp.y = -5;
				transform.localPosition=temp;
			}
		}
	}
}
