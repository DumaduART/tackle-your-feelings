using UnityEngine;
using System.Collections;

public class WingMovement : MonoBehaviour {

	// Use this for initialization
	 public enum MovementType
	 {
		right=0,left
	 };
	public MovementType movementType;
	public bool _ismovement;
	public float movementSpeed;
	public float rightRestriction;
	public float leftRestriction;
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		if(_ismovement)
		{
			switch(movementType)
			{
				case MovementType.right:
				{
					transform.Translate(-Vector3.forward * Time.deltaTime*movementSpeed);
					break;
				}
				case MovementType.left:
				{
					transform.Translate(Vector3.forward * Time.deltaTime*movementSpeed);
					break;
				}
			}
		
		}
		
		if(transform.position.x>=rightRestriction)
			movementType=MovementType.left;
		if(transform.position.x<=leftRestriction)
			movementType=MovementType.right;
	}
}
