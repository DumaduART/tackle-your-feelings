using UnityEngine;
using System.Collections;

public class LevelAssetHandler : MonoBehaviour {

	// Use this for initialization
	public bool iscleared=false;
	//public static LevelAssetHandler instance;
	void Start () {
//		instance=this;
	}
	
	public bool IsAssetCleared()
	{
		return iscleared;
	}
	public void SetAssetCleared(bool x)
	{
		if(x)
			iscleared=false;
		else
			iscleared=true;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
