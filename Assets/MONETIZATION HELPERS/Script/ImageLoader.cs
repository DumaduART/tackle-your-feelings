using UnityEngine;
using System.Collections;
using MonetizationHelper;

namespace MonetizationHelper
{
public class ImageLoader : MonoBehaviour {

	public void LoadFromURL(string url)
	{
		StartCoroutine(LoadImageFromURL(url)); 
	}
	
	IEnumerator LoadImageFromURL(string url)
	{
		WWW www = new WWW(url);
		yield return www;
		gameObject.GetComponent<GUITexture>().texture = www.texture;
	}
}
}