﻿using UnityEngine;
using System.Collections;
using MonetizationHelper;

namespace MonetizationHelper
{
	
public class AchHandler 
{
	static AchHandlerImplementation mInstance = null;
	
	public static AchHandlerImplementation Instance
	{
		get
		{
			if(mInstance == null)
			{
				GameObject tObject = new GameObject();
				tObject.name = "AchievementHandler Instance";
				mInstance = tObject.AddComponent<AchHandlerImplementation>();
			}
			return mInstance;
		}
	}
	
	public static void InstanceDestroyed()
	{
		mInstance = null;
	}
}
}