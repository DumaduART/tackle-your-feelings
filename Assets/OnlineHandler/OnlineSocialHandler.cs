﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using FBKit;

public struct UserData
{
	public string _UserId;
	public string _UserName;
	public string _UserImageURL;
	public double _UserScore;
	public int _UserRank;
}
public struct ScoreData
{
	public string _ScoreUserId;
	public double _Score;
}

public class OnlineSocialHandler : MonoBehaviour 
{
	public IList<UserData> Non_App_FriendsList_Data = new List<UserData>();
	public IList<UserData> App_FriendsList_Data	= new List<UserData>();
	public UserData Current_User_Data = new UserData();
	
	string mstrPermissions = "email,publish_actions",mInviteStr = "",mShareStr = "";
	public string ApiQuery = "";
	bool mbLoginDetails;
	static GameObject mgoOnlineSocialHandler;
	
//	static OnlineSocialHandler mInstance = null;
	private bool isInit = false;
	
	void Start()
	{
		mgoOnlineSocialHandler = gameObject;
		mbLoginDetails = false;
	}
	
	
	public static OnlineSocialHandler Instance
	{
		get
		{
//			if(mInstance == null)
//			{
//				GameObject obj = new GameObject();
//				obj.name = "OnlineSocialHandler";
//				mInstance = obj.AddComponent<OnlineSocialHandler>();
//				DontDestroyOnLoad(obj);
//			}
//			return mInstance;
			return mgoOnlineSocialHandler.GetComponent<OnlineSocialHandler>();
		}
	}
	
//-----------Init-------------
    public void CallFBInit()
    {
		//if(!isInit &&  !FB.IsLoggedIn)
      //	  FB.Init(OnInitComplete, OnHideUnity);
    }

    private void OnInitComplete()
    {
//        Debug.Log("FB.Init completed: Is user logged in? " + FB.IsLoggedIn);
//		if(FB.IsLoggedIn && isInit)
//			return;
//        isInit = true;
//        CallFBLogin();
		 isInit = true;
		if(PlayerPrefs.GetInt("LoggedInStatus") == 0)
			CallFBLogin();
		else if(PlayerPrefs.GetInt("LoggedInStatus") == 2)
			GetUserInfo();
    }
	private void OnHideUnity(bool isGameShown)
    {
        Debug.Log("Is game showing? " + isGameShown);
    }
//------------Login-------------
	private void CallFBLogin()
    {
		Debug.Log("==Log In==");
       // FB.Login(mstrPermissions, LoginCallBack);
    }
//	void LoginCallBack(FBResult result)
//	{
//		string lastResponse = null;
//		if (result.Error != null)
//            lastResponse = "Error Response:\n" + result.Error;
//        else 
//            lastResponse = "Success Response:\n" + result.Text;
//		Debug.Log("==Logged In==" + lastResponse);
////		if(FB.IsLoggedIn)
////		{
////			PlayerPrefs.SetInt("LoggedIn",1);
////			GetUserInfo();
////		}
//		if(FB.IsLoggedIn)
//		{
//			PlayerPrefs.SetInt("LoggedInStatus",2);
//			GetUserInfo();
//		}
//		else
//			OnlineInterfaceHandler.Instance.Receiver("Logged In",eONLINE_REQ_TYPE.NO_INTERNET_CONNECTION);
//	}
//-----------GetUserInfo----------
	public void GetUserInfo()
	{
		//ApiQuery="/me?fields=name,picture.type(large)&access_token="+FB.AccessToken;
		//FB.API(ApiQuery, Facebook.HttpMethod.GET, UserInfoCallBack);
	}
//	void UserInfoCallBack(FBResult result)
//	{
//		string lastResponse = null;
//		
//		if (result.Error != null)
//		{
//			lastResponse = "Error Response:\n" + result.Error;
//			return;
//		}
//        else 
//		{
//            lastResponse = "Success Response:\n" + result.Text;
//		}
//		
//		Debug.Log("==UserInfoCallBack==" + lastResponse);
//		JSONObject JSONData = new JSONObject(result.Text);
//		
//		Current_User_Data._UserId = JSONData.GetField("id").str;
//		Current_User_Data._UserName = JSONData.GetField("name").str;
//		Current_User_Data._UserImageURL = JSONData.GetField("picture").GetField("data").GetField("url").str;
//		string [] strImg;
//		strImg = Current_User_Data._UserImageURL.Split('\\');
//		Current_User_Data._UserImageURL = strImg[0]+strImg[1]+strImg[2]+strImg[3]+strImg[4];
//		Current_User_Data._UserScore = 0;
//		//Debug.Log("==Current_User_Name==" + Current_User_Data._UserName);
//		GetFriendsInfo();
//	}
	//-----------GetFriendsInfo----------
	public void GetFriendsInfo()
	{
		//ApiQuery="/me?fields=friends.fields(name,id,picture.type(large),installed)&access_token="+FB.AccessToken;
		//FB.API(ApiQuery, Facebook.HttpMethod.GET, FriendsInfoCallBack);
	}
//	void FriendsInfoCallBack(FBResult result)
//	{
//		string lastResponse = null;
//		
//		if (result.Error != null)
//		{
//			lastResponse = "Error Response:\n" + result.Error;
//			return;
//		}
//        else 
//		{
//            lastResponse = "Success Response:\n" + result.Text;
//		}
//		
//		Debug.Log("==GetFriendsInfo==" + lastResponse);
//		SetFriendsCategorization(result.Text);
//	}
//	
	void SetFriendsCategorization(string friendList)
	{
		JSONObject JSONData = new JSONObject(friendList);
		ArrayList tArray = JSONData.GetField("friends").GetField("data").list;
			while(App_FriendsList_Data.Count > 0)
				App_FriendsList_Data.RemoveAt(0);	
			while(Non_App_FriendsList_Data.Count > 0)
				Non_App_FriendsList_Data.RemoveAt(0);	
		if(tArray.Count > 0)
		{
			foreach(JSONObject jObj in tArray)
			{
				UserData data = new UserData();
				bool bInstalled = false;
				JSONObject jTempObj = null;
				jTempObj = jObj.GetField("installed");
				if(jTempObj != null)				
				{
					bInstalled =jTempObj.b;
				}
				if(bInstalled)
				{
					data._UserId = jObj.GetField("id").str;
					data._UserName = jObj.GetField("name").str;
					data._UserImageURL = jObj.GetField("picture").GetField("data").GetField("url").str;
					string [] strImg;
					strImg = data._UserImageURL.Split('\\');
					data._UserImageURL = strImg[0]+strImg[1]+strImg[2]+strImg[3]+strImg[4];
					//Debug.Log("====urlSetting=== "+data._UserImageURL);
					data._UserScore = 0;
					App_FriendsList_Data.Add(data);
				}				
				else
				{
					data._UserId = jObj.GetField("id").str;
					data._UserName = jObj.GetField("name").str;
					data._UserImageURL = (jObj.GetField("picture").GetField("data").GetField("url")).str.ToString();
					
					string [] strImg;
					strImg = data._UserImageURL.Split('\\');
					data._UserImageURL = strImg[0]+strImg[1]+strImg[2]+strImg[3]+strImg[4];
					//Debug.Log("====urlSetting=N== "+data._UserImageURL);
					data._UserScore = 0;
					Non_App_FriendsList_Data.Add(data);
				}
			}
		}
		
		foreach(UserData data in App_FriendsList_Data)
		{
		//	Debug.Log("App Friend Name is:"+ data._UserName);
		}
		
		foreach(UserData data in Non_App_FriendsList_Data)
		{
		//	Debug.Log("Non App Friend Name is:"+ data._UserName);
		}		
		mbLoginDetails = true;
		OnlineInterfaceHandler.Instance.Receiver("Logged In",eONLINE_REQ_TYPE.LOGIN);
		
	}		
	
	public bool GetIsLoginDetails()
	{
		return mbLoginDetails;
	}
	
//------------Logout------------
	public void CallLogOut()
	{
//		if(FB.IsLoggedIn)
//		{
//			FB.Logout();
//			
//		}
		PlayerPrefs.SetInt("LoggedInStatus",0);
		OnlineInterfaceHandler.Instance.Receiver("Logged Out",eONLINE_REQ_TYPE.LOGOUT);
		
//		GameObject obj = GameObject.Find("UnityFacebookSDKPlugin");
//		if(obj != null)
//		{
//			Destroy(obj);
//			obj = null;
//		}		
//		if(gameObject != null)
//		{
//			Destroy(gameObject);
//		}
		
		while(App_FriendsList_Data.Count > 0)
			App_FriendsList_Data.RemoveAt(0);	
		while(Non_App_FriendsList_Data.Count > 0)
			Non_App_FriendsList_Data.RemoveAt(0);	
		
	}	
	public void InviteBtnClick(string str)
	{
		mInviteStr = str;
		StartCoroutine(CheckInternetConnectionInvite());
	}
	
	IEnumerator CheckInternetConnectionInvite()
	{
		 float timeTaken = 0.0F;
        float maxTime = 2.0F;
		bool thereIsConnection=true;
        while ( thereIsConnection )
        {
            Ping testPing = new Ping( "74.125.224.72" );
            timeTaken = 0.0F;
            while ( !testPing.isDone )
            {
    	        timeTaken += Time.deltaTime;
                if ( timeTaken > maxTime )
                {
     	             // if time has exceeded the max
                    // time, break out and return false
                    thereIsConnection = false;
					//Debug.Log("--No InterNet--"+timeTaken);
					break;
                }
				yield return null;
            }
            if ( timeTaken <= maxTime ) 
			{
				thereIsConnection = true;
				//Debug.Log("--InterNet--");
				CallInvite(mInviteStr);
				break;
			}
			if(!thereIsConnection)
			{
				CallNoInternetConnection();
				break;
			}
            yield return null;
		}
	}
	public void ShareBtnClick(string str)
	{
		mShareStr = str;
		StartCoroutine(CheckInternetConnectionShare());
	}
	
	IEnumerator CheckInternetConnectionShare()
	{
		 float timeTaken = 0.0F;
        float maxTime = 2.0F;
		bool thereIsConnection=true;
        while ( thereIsConnection )
        {
            Ping testPing = new Ping( "74.125.224.72" );
            timeTaken = 0.0F;
            while ( !testPing.isDone )
            {
    	        timeTaken += Time.deltaTime;
                if ( timeTaken > maxTime )
                {
     	             // if time has exceeded the max
                    // time, break out and return false
                    thereIsConnection = false;
					//Debug.Log("--No InterNet--"+timeTaken);
					break;
                }
				yield return null;
            }
            if ( timeTaken <= maxTime ) 
			{
				thereIsConnection = true;
				//Debug.Log("--InterNet--");
				CallShareOnWall(mShareStr);
				break;
			}
			if(!thereIsConnection)
			{
				CallNoInternetConnection();
				break;
			}
            yield return null;
		}
	}
	
	
	
//------------Invite------------
	public void CallInvite(string str)
	{
		try
		{
			OnlineInterfaceHandler.Instance.Receiver("Invite",eONLINE_REQ_TYPE.INVITE_FRIENDS);
			JSONObject jObj = new JSONObject(str);
			
			string FriendSelectorTitle = jObj.GetField("title").str;
	   		string FriendSelectorMessage = jObj.GetField("message").str;
	    	string FriendSelectorFilters = jObj.GetField("filters").str;
	    	string FriendSelectorData = jObj.GetField("data").str;
	    	string FriendSelectorMax = jObj.GetField("maxRecipients").str;	
			string FriendSelectorExcludeIds = "";
			string FriendSelectorId = jObj.GetField("id").str;
			string[] excludeIds = (FriendSelectorExcludeIds == "") ? null : FriendSelectorExcludeIds.Split(',');
	
			int? maxRecipients = null;
	        if(jObj.GetField("maxRecipients").str != "") 
	        {
	        	maxRecipients = int.Parse(FriendSelectorMax);                               
	        }
			
			string [] ids=new string[1];
			ids[0]=FriendSelectorId;
			if(FriendSelectorId == "")
			{
//				FB.AppRequest(
//		                    message: FriendSelectorMessage,
//							filters: FriendSelectorFilters,
//		                    excludeIds: excludeIds,
//		                    maxRecipients: maxRecipients,
//		                    data: FriendSelectorData,
//		                    title: FriendSelectorTitle,
//		                    callback: null
//		                );
			}
			else
			{
//				FB.AppRequest(
//	                    message: FriendSelectorMessage,
//	            		to:ids,
//						filters: FriendSelectorFilters,
//	                    excludeIds: excludeIds,
//	                    maxRecipients: maxRecipients,
//	                    data: FriendSelectorData,
//	                    title: FriendSelectorTitle,
//	                    callback: null
//	                );
			}
		}
		catch(Exception e)
		{
			//Debug.Log(e.ToString());
		}
			
	}

//-----------Share--------------
	public void CallShareOnWall(string str)
	{
		try
		{
			OnlineInterfaceHandler.Instance.Receiver("Share",eONLINE_REQ_TYPE.SHARE_ON_WALL);
			JSONObject jObj = new JSONObject(str); 
			
//			FB.Feed(		
//				toId: "",
//	            link: jObj.GetField("link").str,
//	            linkName: jObj.GetField("linkName").str,
//	            linkCaption: jObj.GetField("linkCaption").str,
//	            linkDescription: jObj.GetField("linkDescription").str,
//	            picture: "",
//	            mediaSource: "",
//	            actionName: "",
//	            actionLink: "",
//	            reference: "",
//	            properties:new Dictionary<string, string[]>(),
//	            callback: null);
		}
		catch(Exception e)
		{
			//Debug.Log(e.ToString());
		}
	}
	
//------------Share Screen Shot----------
	public void ShareScreenShot()
	{
		StartCoroutine(TakeScreenshot());
		//Debug.Log("ShareScreenShot");
	}
	
	private IEnumerator TakeScreenshot() 
    {
        yield return new WaitForEndOfFrame();

        var width = Screen.width;
        var height = Screen.height;
        var tex = new Texture2D(width, height, TextureFormat.RGB24, false);
        // Read screen contents into the texture
        tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
        tex.Apply();
        byte[] screenshot = tex.EncodeToPNG();

        var wwwForm = new WWWForm();
        wwwForm.AddBinaryData("image", screenshot, "InteractiveConsole.png");
        wwwForm.AddField("message", "herp derp.  I did a thing!  Did I do this right?");

        //FB.API("me/photos", Facebook.HttpMethod.POST, null, wwwForm);
    }
	
//	public bool GetIsLoggedIn()
//	{
//		//return FB.IsLoggedIn;
//	}
	
//TestInternetConnection
	public void CheckInternet()
	{
		StartCoroutine(TestConnection());
	}
	IEnumerator TestConnection()
    {
        float timeTaken = 0.0F;
        float maxTime = 2.0F;
		bool thereIsConnection=true;
        while ( thereIsConnection )
        {
            Ping testPing = new Ping( "74.125.224.72" );
            timeTaken = 0.0F;
            while ( !testPing.isDone )
            {
    	        timeTaken += Time.deltaTime;
                if ( timeTaken > maxTime )
                {
     	             // if time has exceeded the max
                    // time, break out and return false
                    thereIsConnection = false;
					//Debug.Log("--No InterNet--"+timeTaken);
					break;
                }
				yield return null;
            }
            if ( timeTaken <= maxTime ) 
			{
				thereIsConnection = true;
				//Debug.Log("--InterNet--");
//				if(!FB.IsInitialized)
//					CallFBInit();	
//				else
//					CallFBLogin();
				break;
			}
			if(!thereIsConnection)
			{
				CallNoInternetConnection();
				break;
			}
            yield return null;
		}
    }
	public void CallNoInternetConnection()
	{
		//Debug.Log("====Check ur Internet Connection====");
		OnlineInterfaceHandler.Instance.Receiver("====Check ur Internet Connection====",eONLINE_REQ_TYPE.NO_INTERNET_CONNECTION);
	}
//Getters	
	public string GetCurrentUserId()	
	{
		return Current_User_Data._UserId;
	}
	public IList<string> GetAppFriendsIdsWithCurrentUserId()
	{
		IList<string> strList = new List<string>();
		for(int i=0;i<App_FriendsList_Data.Count;i++)
		{
			if(App_FriendsList_Data[i]._UserId == null || App_FriendsList_Data[i]._UserId == "")
			{
				strList.Add("0");
			}
			else 
				strList.Add(App_FriendsList_Data[i]._UserId);
		}
		if(Current_User_Data._UserId == null || Current_User_Data._UserId == "")
		{
			strList.Add("0");
		}
		else 
			strList.Add(Current_User_Data._UserId);
		
		return strList;
	}
	public IList<UserData> GetAppFriendsData()
	{
		return App_FriendsList_Data;
	}
	public IList<UserData> GetNonAppFriendsData()
	{
		return Non_App_FriendsList_Data;
	}
	public UserData GetCurrentUserData()
	{
		return Current_User_Data;
	}
	
//--------SortedListOfAppfriends----
	public void SetSortedListofAppFriends(IList<ScoreData> Sorted_List_of_Friends)
	{
		//Debug.Log("SetSortedListofAppFriends=="+OnlineInterfaceHandler.Instance.GetRequestType());
		IList<UserData> SortedListofFriends = new List<UserData>();
		
		for(int i=0; i<Sorted_List_of_Friends.Count;i++)
		{
			if(Sorted_List_of_Friends[i]._ScoreUserId != Current_User_Data._UserId)
			{
				for(int n=0;n<App_FriendsList_Data.Count;n++)
				{
					if(Sorted_List_of_Friends[i]._ScoreUserId ==App_FriendsList_Data[n]._UserId)
					{
						UserData data = new UserData();
						data._UserId = App_FriendsList_Data[n]._UserId;
						data._UserImageURL = App_FriendsList_Data[n]._UserImageURL;
						data._UserScore = Sorted_List_of_Friends[i]._Score;
						data._UserName = App_FriendsList_Data[n]._UserName;
						data._UserRank = i+1;
						SortedListofFriends.Add(data);	
					}
				}
			}
			else
			{
				UserData data = new UserData();
				data._UserId = Current_User_Data._UserId;
				data._UserImageURL = Current_User_Data._UserImageURL;
				data._UserScore = Sorted_List_of_Friends[i]._Score;
				data._UserName = Current_User_Data._UserName;
				data._UserRank = i+1;
				Current_User_Data._UserRank = i+1;
				SortedListofFriends.Add(data);	
			}
		}
		if(OnlineInterfaceHandler.Instance.GetRequestType() == eONLINE_REQ_TYPE.GET_TOP_RANKING_LIST)
			OnlineInterfaceHandler.Instance.Receiver("",eONLINE_REQ_TYPE.GET_TOP_RANKING_LIST,SortedListofFriends);
		else if(OnlineInterfaceHandler.Instance.GetRequestType() == eONLINE_REQ_TYPE.GET_TOP_N_RANKING_LIST)
			Set_List_of_N_Users(OnlineInterfaceHandler.Instance.GetNoofUserListNeeded(),SortedListofFriends);
		else if(OnlineInterfaceHandler.Instance.GetRequestType() == eONLINE_REQ_TYPE.GET_CURRENT_USER_RANK)
			SetCurrentUserRank();
		else if(OnlineInterfaceHandler.Instance.GetRequestType() == eONLINE_REQ_TYPE.GET_TOP_RANKER)
		{
			if(SortedListofFriends.Count>0)
				SetTopRanker(SortedListofFriends);
		}
		
	}	
	
	void SetCurrentUserRank()
	{
		OnlineInterfaceHandler.Instance.Receiver(Current_User_Data._UserRank.ToString(),eONLINE_REQ_TYPE.GET_CURRENT_USER_RANK);
	}
	void SetTopRanker(IList<UserData>  TopFriendData)
	{
		//Debug.Log("SetTopRanker");
		OnlineInterfaceHandler.Instance.Receiver(Current_User_Data._UserRank.ToString(),eONLINE_REQ_TYPE.GET_TOP_RANKER,TopFriendData);
	}
	
	void Set_List_of_N_Users(int N, IList<UserData> Sorted_List_of_Friends)
	{
		//Debug.Log("N == "+N);
		IList<UserData> SortedListof_N_Friends = new List<UserData>();
		if(Current_User_Data._UserRank <= N-1)
		{
			for(int i=0;i<N && i<Sorted_List_of_Friends.Count;i++)
			{
				SortedListof_N_Friends.Add(Sorted_List_of_Friends[i]);				
			}
		}
		else
		{
			for(int i=0; i<N-3 && i<Sorted_List_of_Friends.Count;i++)
			{
				SortedListof_N_Friends.Add(Sorted_List_of_Friends[i]);
			}
			SortedListof_N_Friends.Add(Sorted_List_of_Friends[Current_User_Data._UserRank - 2]);
			SortedListof_N_Friends.Add(Sorted_List_of_Friends[Current_User_Data._UserRank - 1]);
			if(Sorted_List_of_Friends.Count > Current_User_Data._UserRank)
			{
				SortedListof_N_Friends.Add(Sorted_List_of_Friends[Current_User_Data._UserRank]);
			}
		}
		OnlineInterfaceHandler.Instance.Receiver("",eONLINE_REQ_TYPE.GET_TOP_N_RANKING_LIST,SortedListof_N_Friends);
	}
}
