using UnityEngine;
using System.Collections;

public class PowerUpGUIItemsManager : GUIItemsManager
{
	public GameObject MainBackGround;	
	
	public Texture2D[] _BallTexture;
	public GUITexture[] _guiTexture;
	public GameObject subscreenmanager;
	ScreenManager subscreenitemmanager;	
	public GameObject gametips;
	GameObject gearbuttondeactivated = null;
	private GameManager _gameManager;
	public int index = 1 ;
	bool tutorial_level = false;
	
	public GameObject LeftButton;
	public GameObject RightButton;
	
	// Use this for initialization
	void Start ()
	{
		base.Init();
		index=1;
		_gameManager=GameObject.Find("GameManager").GetComponent("GameManager") as GameManager ;
		_guiTexture[0].texture = _BallTexture[index-1];
		_guiTexture[1].texture = _BallTexture[index];
		_guiTexture[2].texture = _BallTexture[index+1];
	}
	public void OnSubScreenSelectedEvent(GUIItem item)
	{
		//Debug.Log(item.name + "here");
		
		if(meState != eGUI_ITEMS_MANAGER_STATE.Settled)
			return;
				
	}	
	public override void OnSelectedEvent(GUIItem item)
	{
		if(meState == eGUI_ITEMS_MANAGER_STATE.Settled && _screenManager != null)
		{
			//Debug.Log("----- In Derived class event handle : " + item.name);
			if(item.name == "LeftBtn")
			{
				if(index== _BallTexture.Length-1 )
					RightButton.SetActive(true);
				if(index > 0)
				index--;
				
				_guiTexture[1].texture = _BallTexture[index];
				_guiTexture[2].texture = _BallTexture[index+1];
				if(index>0)
				_guiTexture[0].texture = _BallTexture[index-1];  
				else 
				{
					_guiTexture[0].texture = null ; 
					LeftButton.SetActive(false);
				}
			}
			
			if(item.name == "RightBtn")
			{
				if(index== 0 )
					LeftButton.SetActive(true);
				if(index < _BallTexture.Length-1)
				index++;
				_guiTexture[1].texture = _BallTexture[index];
				_guiTexture[0].texture = _BallTexture[index-1];
				if(index < _BallTexture.Length-1)
				 _guiTexture[2].texture = _BallTexture[index+1];
				else 
				{
					 _guiTexture[2].texture = null;
					RightButton.SetActive(false);
				}
			}
		
		}
		
		
		
	}	
	
	
}