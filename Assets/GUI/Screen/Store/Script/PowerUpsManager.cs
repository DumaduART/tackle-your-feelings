using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum PowerUpType
{
	    None=0 , FireShot, CannonBall,PaperSoul,Stickiee
};

[System.Serializable]
public class PowerUp
{
	public string _PowerupName;
	public bool _isAvailable;
	public PowerUpType _poweruptype;
	public int _count;
	public Texture2D _PowerupTexture;
 //	public GameObject _powerupreference;
	
}


public class PowerUpsManager : MonoBehaviour {
	
	public static PowerUpsManager instance;
	public PowerUp[] _noOfPowerUp ;
	public List<PowerUp> _SelectedPowerups = new List<PowerUp>();
	public int maxiMumNoOfPower;
	private int noOfPowerSelection;

	// Use this for initialization
	void Start () {
		instance = this;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void AddSelectedPowerUp(PowerUpType powertype)
	{
		
		foreach(PowerUp power in _noOfPowerUp)
		{
			 bool isAdded = false;
			if(power._poweruptype == powertype && power._count>0)
			{
				foreach(PowerUp p in _SelectedPowerups)
				{
					if(p==power)
					{
						 isAdded = true;			
						 break;		
					}
				}
				
				if(!isAdded)
				{
					if(noOfPowerSelection < maxiMumNoOfPower)
					{
						_SelectedPowerups.Add(power);
						noOfPowerSelection++ ;
					}
				}
				else
				{
					_SelectedPowerups.Remove(power);
					noOfPowerSelection-- ;
				}	
			}
	    }
	}
	
	public void PurchaseItem(PowerUpType powertype )
	{
		
		foreach(PowerUp power in _noOfPowerUp)
		{
			if(power._poweruptype == powertype)
			{
				power._count++;
				//guitext.text = power._count.ToString();
			}
		}
	}
}
