using UnityEngine;
using System.Collections;

public class StatsScreenGUIItemsManager : GUIItemsManager
{
	public GameObject MainBackGround;	

	public GameObject subscreenmanager;
	ScreenManager subscreenitemmanager;	
	public GameObject gametips;
	GameObject gearbuttondeactivated = null;
	private GameManager _gameManager;
	
	bool tutorial_level = false;
	
	bool _isAndroidBack = false;
	
	int _gameTime;
	int _chkTime = 1;
	
	// Use this for initialization
	void Start ()
	{
		//Instantiate(subscreenmanager);
		
		base.Init();	
		_gameManager=GameObject.Find("GameManager").GetComponent("GameManager") as GameManager ;
		subscreenitemmanager = GameObject.Find("ShopSubScreenManager").GetComponent<ScreenManager>();
		subscreenitemmanager.LoadScreen(subscreenitemmanager.defaultScreen);
	}
	
	void Update()
	{
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			if(_isAndroidBack)
			{
				subscreenitemmanager.LoadScreen("Empty");
				_screenManager.LoadScreen("MainMenu");
				_isAndroidBack = false;
			}
		}
		_gameTime = (int)Time.time;
		if(_gameTime >= _chkTime)
		{
			_chkTime = _gameTime+1;
			GuiTextManager.instance.UpdateText();
		}
	}
	
	public override void OnEntryAnimationCompleted()
	{
		_isAndroidBack = true;
	}
	
	public void OnSubScreenSelectedEvent(GUIItem item)
	{
		//Debug.Log(item.name + "here");
		
		if(meState != eGUI_ITEMS_MANAGER_STATE.Settled)
			return;
				
	}	
	public override void OnSelectedEvent(GUIItem item)
	{
		if(meState == eGUI_ITEMS_MANAGER_STATE.Settled && _screenManager != null)
		{
			//Debug.Log("----- In Derived class event handle : " + item.name);
			if(item.name == "BackBtn")
			{
				subscreenitemmanager.LoadScreen("Empty");
				_screenManager.LoadScreen("MainMenu");
				
				//GameObject.Find("ScrollSubScreenManagr(Clone)").GetComponent<ScreenManager>().closeScreenManager();
			}
		}
		
		
		
	
	}	
}