using UnityEngine;
using System.Collections;

public class PopUpScreen : GUIItemsManager
{
	public enum AccuracyType
	{
		good,awesome,great
	};
	public AccuracyType accuracyType;
	public GameObject MainBackGround;	
	public GameObject ring;
	public GameObject subscreenmanager;
	ScreenManager subscreenitemmanager;	
	public GameObject gametips;
	GameObject gearbuttondeactivated = null;
	PopUpManager _popUpManager;
	
	bool tutorial_level = false;
	
	
	
	// Use this for initialization
	void Start ()
	{
		_popUpManager = GameObject.Find("GameManager").GetComponent<PopUpManager>();
		_popUpManager.AddToList(this.gameObject);
		base.Init();	
	}
	public void OnSubScreenSelectedEvent(GUIItem item)
	{
		//Debug.Log(item.name + "here");
		
		if(meState != eGUI_ITEMS_MANAGER_STATE.Settled)
			return;
				
	}	
	public override void OnSelectedEvent(GUIItem item)
	{
		if(meState == eGUI_ITEMS_MANAGER_STATE.Settled && _screenManager != null)
		{
//			Debug.Log("----- In Derived class event handle : " + item.name);
			
		}
		
		
		
		
	}	
	
	public override void OnEntryAnimationCompleted()
	{
		this.onExitCompleteCallBack = managerExitCallBack;
		this.State = eGUI_ITEMS_MANAGER_STATE.Exiting;
		
	}
	
	void managerExitCallBack()
	{
		_popUpManager.RemoveFromList(this.gameObject);
		Destroy(this.gameObject);
		
	}
	
}