﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



[System.Serializable]
public class CategoryContent
{
	public string categoryDescription;
	public string gameOverCondition;
	public string GoldDescription;
	public string silverDescription;
	public string bronzeDescription;
}

public class CategoryDescription : MonoBehaviour {

	// Use this for initialization
	public GUIText[] _guitext;
	public CategoryContent _categoryContent;
	
	void Start () {
		_guitext[0].text = _categoryContent.categoryDescription;
		_guitext[1].text = _categoryContent.gameOverCondition;
		_guitext[2].text = _categoryContent.GoldDescription;
		_guitext[3].text = _categoryContent.silverDescription;
		_guitext[4].text = _categoryContent.bronzeDescription;
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
