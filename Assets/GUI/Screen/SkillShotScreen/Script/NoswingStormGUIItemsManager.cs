using UnityEngine;
using System.Collections;

public class NoswingStormGUIItemsManager : GUIItemsManager
{
	public GameObject MainBackGround;	

	public GameObject subscreenmanager;
	ScreenManager subscreenitemmanager;	
	public GameObject gametips;
	GameObject gearbuttondeactivated = null;
	private GameManager _gameManager;
	public static NoswingStormGUIItemsManager instance;
	bool tutorial_level = false;
	
	// Use this for initialization
	void Start ()
	{
		instance = this;
		base.Init();	
		_gameManager=GameObject.Find("GameManager").GetComponent("GameManager") as GameManager ;
		//transform.GetComponent<ButtonList>().SetSelectedState("Challenge1");
	}
	public void OnSubScreenSelectedEvent(GUIItem item)
	{
		Debug.Log(item.name + "here");
		
		if(meState != eGUI_ITEMS_MANAGER_STATE.Settled)
			return;
				
	}	
	public override void OnSelectedEvent(GUIItem item)
	{
		if(meState == eGUI_ITEMS_MANAGER_STATE.Settled && _screenManager != null)
		{
			Debug.Log("----- In Derived class event handle : " + item.name);
			if(item.name == "BackBtn")
			{
				_screenManager.LoadScreen("MainMenu");
				EventManager.GameHomeTrigger();
			}
			if(item.name == "LeftButton")
			{
				_screenManager.LoadScreen("HitTheBar");
				_gameManager.SetCategory(GameManager .Category.HitTheBar);
				//_gameManager.LoadChallenge(_gameManager.GetIndex());
				//Destroy(_gameManager.challenge);
			}
			if(item.name == "RightButton")
			{
				print (item.name);
				if(_gameManager.GetCoins() >= 2000)
				{
					_screenManager.LoadScreen("ThroughRings");
					
				}
				
				else
					_screenManager.LoadScreen("ThroughRingsLocked");
				_gameManager.SetCategory(GameManager .Category.ThroughRings);
				//_gameManager.LoadChallenge(_gameManager.GetIndex());
				//Destroy(_gameManager.challenge);
			}
			if(item.name == "Challenge1")
			{
				ButtonList.instance.SetSelectedState(item.name,true);
				_gameManager.LoadChallenge(0);
				_gameManager.SetIndex(0);
				Instantiate(subscreenmanager);
				SetActive(false);
			}
			if(item.name == "Challenge2")
			{
				ButtonList.instance.SetSelectedState(item.name,true);
				_gameManager.LoadChallenge(1);
				_gameManager.SetIndex(1);
				Instantiate(subscreenmanager);
				SetActive(false);
			}
			if(item.name == "Challenge3")
			{
				ButtonList.instance.SetSelectedState(item.name,true);
				_gameManager.LoadChallenge(2);
				_gameManager.SetIndex(2);
				Instantiate(subscreenmanager);
				SetActive(false);
			}
			if(item.name == "Challenge4")
			{
				ButtonList.instance.SetSelectedState(item.name,true);
				_gameManager.LoadChallenge(3);
				_gameManager.SetIndex(3);
				Instantiate(subscreenmanager);
				SetActive(false);
			}
			if(item.name == "Challenge5")
			{
				ButtonList.instance.SetSelectedState(item.name,true);
				_gameManager.LoadChallenge(4);
				_gameManager.SetIndex(4);
				Instantiate(subscreenmanager);
				SetActive(false);
			}
			if(item.name == "Challenge6")
			{
				ButtonList.instance.SetSelectedState(item.name,true);
				_gameManager.LoadChallenge(5);
				_gameManager.SetIndex(5);
				Instantiate(subscreenmanager);
				SetActive(false);
			}
			if(item.name == "Challenge7")
			{
				ButtonList.instance.SetSelectedState(item.name,true);
				print (item.name);
				_gameManager.LoadChallenge(6);
				_gameManager.SetIndex(6);
				Instantiate(subscreenmanager);
				SetActive(false);
			}
			if(item.name == "Challenge8")
			{
				ButtonList.instance.SetSelectedState(item.name,true);
				print (item.name);
				_gameManager.LoadChallenge(7);
				_gameManager.SetIndex(7);
				Instantiate(subscreenmanager);
				SetActive(false);
			}
			if(item.name == "Challenge9")
			{
				ButtonList.instance.SetSelectedState(item.name,true);
				print (item.name);
				_gameManager.LoadChallenge(8);
				_gameManager.SetIndex(8);
				Instantiate(subscreenmanager);
				SetActive(false);
			}
			if(item.name == "Challenge10")
			{
				ButtonList.instance.SetSelectedState(item.name,true);
				print (item.name);
				_gameManager.LoadChallenge(9);
				_gameManager.SetIndex(9);
				Instantiate(subscreenmanager);
				SetActive(false);
			}
			if(item.name == "Challenge11")
			{
				ButtonList.instance.SetSelectedState(item.name,true);
				print (item.name);
				_gameManager.LoadChallenge(10);
				_gameManager.SetIndex(10);
				Instantiate(subscreenmanager);
				SetActive(false);
			}
			if(item.name == "Challenge12")
			{
				ButtonList.instance.SetSelectedState(item.name,true);
				print (item.name);
				_gameManager.LoadChallenge(11);
				_gameManager.SetIndex(11);
				Instantiate(subscreenmanager);
				SetActive(false);
			}
			if(item.name == "PlayBtn")
			{
				if(_gameManager.challenge != null)
				{
					LevelHandler levelhandler = LevelHandler.instance;
					if(levelhandler._gameOverParameter == LevelHandler.GameoverParameter.ball)
					{
						_screenManager.LoadScreen("ArcadeGamePlay");
					}
					
					if(levelhandler._gameOverParameter == LevelHandler.GameoverParameter.time)
					{
						_screenManager.LoadScreen("SkillShot");
					}
					EventManager.GameStartTrigger();
				}
			}
		}
		
		
		
	}	
	
	public void SetActive(bool x)
	{
		if(x)
			//this.enabled = true;
			GameObject.Find("ScreenManager").GetComponent<ScreenManager>().EnableInput ();
		else
			GameObject.Find("ScreenManager").GetComponent<ScreenManager>().DisableInput ();
		
		
		
	}
}