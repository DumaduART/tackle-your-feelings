﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



[System.Serializable]
public class BallType
{
	public string name;
	public bool isAvailable;
	public bool isSelected;
	public Texture2D ballTexture;
	public int CostToUnlock;
	public int speedInAir;
	public int windResistance;
	public int SwingAngel;
	public int noOfBallInPak;
	public int noOfBallAvailable;
	
}

public class BallSelectionManager : MonoBehaviour {
	
	public static BallSelectionManager instance;
	public BallType[] _ballType;
	public  GameManager gameManager;
	// Use this for initialization
	void Awake()
	{
		instance = this;
	}
	void Start () {
		
		gameManager = GameManager.GetInstance();
			
		for(int i=0;i<_ballType.Length;i++)
			{
				if(gameManager.UpdateIntDataInDevice("_ballTypeisAvailable"+i,1,true) == 1)
				{
					_ballType[i].isAvailable = true;
				}
				
				else
				{
					_ballType[i].isAvailable = false;
				}
				
				if(gameManager.UpdateIntDataInDevice("_ballTypeisSelected"+i,1,true) == 1)
				{
					_ballType[i].isSelected = true;
				}
				
				else
				{
					_ballType[i].isSelected = false;
				}
			}
		
		for(int i=0;i<_ballType.Length;i++)
			{
				_ballType[i].noOfBallAvailable = gameManager.UpdateIntDataInDevice("noOfBallAvailable"+i,
				_ballType[i].noOfBallAvailable,true);
			}
	}
	
	public void UpdateInDevice()
	{
		
			for(int i=0;i<_ballType.Length;i++)
				{
					
					if(_ballType[i].isAvailable)
					{
						gameManager.UpdateIntDataInDevice("_ballTypeisAvailable"+i,1,false);
					}
					
					if(_ballType[i].isSelected)
					{
						gameManager.UpdateIntDataInDevice("_ballTypeisSelected"+i,1,false);
					}
				}
	}
	
	public void UnlockBall(int id)
	{
		int index = id -1;
		switch(id)
		{
			case 1:
			{	
				//int index = 0;
				if(_ballType[index].isAvailable)
				{
					_ballType[index].isSelected = true;
					for(int i =0 ;i<_ballType.Length;i++)
					{
						if(i != index)
						{
							_ballType[i].isSelected = false;
				
						}
					}
				}
				else
				{
					if(gameManager.GetCoins() >= _ballType[index].CostToUnlock)
					{
						gameManager.AddCoin(-_ballType[index].CostToUnlock);
						_ballType[index].isAvailable = true;
		
					}
					
					else
					{
						
					}
				}
				break;
			}
			
			case 2:
			{
				//int index = 1;
				if(_ballType[index].isAvailable)
				{
					_ballType[index].isSelected = true;
					for(int i =0 ;i<_ballType.Length;i++)
					{
						if(i != index)
						{
							_ballType[i].isSelected = false;
				
						}
					}
				}
				else
				{
					if(gameManager.GetCoins() >= _ballType[index].CostToUnlock)
					{
						gameManager.AddCoin(-_ballType[index].CostToUnlock);
						_ballType[index].isAvailable = true;
		
					}
					
					else
					{
						
					}
				}
				break;
			}
			
			case 3:
			{
				//int index = 2;
				if(_ballType[index].isAvailable)
				{
					_ballType[index].isSelected = true;
					for(int i =0 ;i<_ballType.Length;i++)
					{
						if(i != index)
						{
							_ballType[i].isSelected = false;
				
						}
					}
				}
				else
				{
					if(gameManager.GetCoins() >= _ballType[index].CostToUnlock)
					{
						gameManager.AddCoin(-_ballType[index].CostToUnlock);
						_ballType[index].isAvailable = true;
		
					}
					
					else
					{
						
					}
				}
				break;
			}
			
			case 4:
			{
				//int index = 3;
				if(_ballType[index].isAvailable)
				{
					_ballType[index].isSelected = true;
					for(int i =0 ;i<_ballType.Length;i++)
					{
						if(i != index)
						{
							_ballType[i].isSelected = false;
				
						}
					}
				}
				else
				{
					if(gameManager.GetCoins() >= _ballType[index].CostToUnlock)
					{
						gameManager.AddCoin(-_ballType[index].CostToUnlock);
						_ballType[index].isAvailable = true;
		
					}
					
					else
					{
						
					}
				}
				break;			}
			
			case 5:
			{
				//int index = 4;
				if(_ballType[index].isAvailable)
				{
					_ballType[index].isSelected = true;
					for(int i =0 ;i<_ballType.Length;i++)
					{
						if(i != index)
						{
							_ballType[i].isSelected = false;
				
						}
					}
				}
				else
				{
					if(gameManager.GetCoins() >= _ballType[index].CostToUnlock)
					{
						gameManager.AddCoin(-_ballType[index].CostToUnlock);
						_ballType[index].isAvailable = true;
		
					}
					
					else
					{
						
					}
				}
				break;
			}
			
		}
		
		// saving the ball selection state 
			for(int i=0;i<_ballType.Length;i++)
			{
				
				if(_ballType[i].isAvailable)
				{
					gameManager.UpdateIntDataInDevice("_ballTypeisAvailable"+i,1,false);
				}
				
				else
				{
					gameManager.UpdateIntDataInDevice("_ballTypeisAvailable" +i,0,false);
				}
				
				if(_ballType[i].isSelected)
				{
					gameManager.UpdateIntDataInDevice("_ballTypeisSelected"+i,1,false);
				}
				
				else
				{
					gameManager.UpdateIntDataInDevice("_ballTypeisSelected"+i,0,false);
					
				}
					
			}
		
	}
	
	public static BallSelectionManager GetInstance()
	{
		return(instance);
	}
	
	public void IncreaseBallAvailable(int index)
	{
		_ballType[index].noOfBallAvailable += _ballType[index].noOfBallInPak;
		gameManager.UpdateIntDataInDevice("noOfBallAvailable"+index,_ballType[index].noOfBallAvailable,false);
	}
}
