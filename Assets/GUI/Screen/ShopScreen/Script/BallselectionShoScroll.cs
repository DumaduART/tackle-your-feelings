using UnityEngine;
using System.Collections;

public class BallselectionShoScroll : ScrollAndPaging
{
	GUIText ShopballText;
	GUIText EnvironmentText;
	GUIText AddupsText;
	ScreenManager subscreenitemmanager;	
	ScreenManager screenManager;
	//public static ShoScroll instance;
	
	// Use this for initialization
	public GameObject[] DisableForCustom;
	BallSelectionManager ballSelectionManager;
	GameManager gameManager;
	
	public GUIText[] ballUnlockingCost;
	public GUIText[] ButtonText;
	public GUIText[] NoOfballText;
	public GUIText[] _noOfBallInpack;
	public GUIText[] _descriptionText;
	
	string Unlock = "UNLOCL";
	string _select = "SELECT";
	string selected = "SELECTED";
			
	void Start () 
	{
		base.InitScroll();		
//		instance = this;
		ballSelectionManager = BallSelectionManager.GetInstance();
		gameManager = GameManager.GetInstance();
		subscreenitemmanager = GameObject.Find("ShopSubScreenManager").GetComponent<ScreenManager>();
		screenManager =  GameObject.Find("ScreenManager").GetComponent<ScreenManager>();
//		for(int i = 0 ; i< ballSelectionManager._ballType.Length;i++)
//		{
//			ballUnlockingCost[i].text = ballSelectionManager._ballType[i].CostToUnlock.ToString();
//			if(ballSelectionManager._ballType[i].isAvailable)
//			{
//				if(ballSelectionManager._ballType[i].isSelected)
//				{
//					ButtonText[i].text =  selected;
//				}
//				
//				else
//				{
//					ButtonText[i].text =  _select;
//				}
//			}
//			
//			else
//			{
//				ButtonText[i].text =  Unlock;
//			}
			
			
			
//		}
		UpdateText();
	}
	
	public override void OnSelectedEvent(GUIItem item)
	{				
		
		if(meState == eGUI_ITEMS_MANAGER_STATE.Settled && _screenManager != null)
		{	
			if(item.name == "Buy1")
			{
				//UnlockBall(1);
				BuyBall(1);
				UpdateText();
			}
			
			if(item.name == "Buy2")
			{
				//UnlockBall(2);
				BuyBall(2);
				UpdateText();
			}
			
			if(item.name == "Buy3")
			{
				//UnlockBall(3);
				BuyBall(3);
				UpdateText();
			}
			
			if(item.name == "UnlockBall4")
			{
				//UnlockBall(4);
			}
			
			if(item.name == "UnlockBall5")
			{
				//UnlockBall(5);
			}
			if(item.name == "VIdeo")
			{

			}
		}
	}
	void ShowVideoAds()
	{
				//Debug.Log("video..");
				GameManager.instance.AddCoin (50);
				GuiTextManager.instance.UpdateText();
	}
	public void UnlockBall(int id)
	{
		int index = id -1;
		switch(id)
		{
			case 1:
			{	
				//int index = 0;
				if(ballSelectionManager._ballType[index].isAvailable)
				{
					ballSelectionManager._ballType[index].isSelected = true;
					ButtonText[index].text = selected;
					for(int i =0 ;i<ballSelectionManager._ballType.Length;i++)
					{
						if(i != index)
						{
							ballSelectionManager._ballType[i].isSelected = false;
							if(ballSelectionManager._ballType[i].isAvailable)
							ButtonText[i].text = _select;
						}
					}
				}
				else
				{
					if(gameManager.GetCoins() >= ballSelectionManager._ballType[index].CostToUnlock)
					{
						gameManager.AddCoin(-ballSelectionManager._ballType[index].CostToUnlock);
						ballSelectionManager._ballType[index].isAvailable = true;
						ButtonText[index].text = _select;
					}
					
					else
					{
						
					}
				}
				break;
			}
			
			case 2:
			{
				//int index = 1;
				if(ballSelectionManager._ballType[index].isAvailable)
				{
					ballSelectionManager._ballType[index].isSelected = true;
					ButtonText[index].text = selected;
					for(int i =0 ;i<ballSelectionManager._ballType.Length;i++)
					{
						if(i != index)
						{
							ballSelectionManager._ballType[i].isSelected = false;
							if(ballSelectionManager._ballType[i].isAvailable)
								ButtonText[i].text = _select;
						}
					}
				}
				else
				{
					if(gameManager.GetCoins() >= ballSelectionManager._ballType[index].CostToUnlock)
					{
						gameManager.AddCoin(-ballSelectionManager._ballType[index].CostToUnlock);
						ballSelectionManager._ballType[index].isAvailable = true;
						ButtonText[index].text = _select;
					}
					
					else
					{
						
					}
				}
				break;
			}
			
			case 3:
			{
				//int index = 2;
				if(ballSelectionManager._ballType[index].isAvailable)
				{
				    
					ballSelectionManager._ballType[index].isSelected = true;
					ButtonText[index].text = selected;
					for(int i =0 ;i<ballSelectionManager._ballType.Length;i++)
					{
						if(i != index)
						{
							ballSelectionManager._ballType[i].isSelected = false;
							if(ballSelectionManager._ballType[i].isAvailable)
								ButtonText[i].text = _select;
						}
					}
				}
				else
				{
					if(gameManager.GetCoins() >= ballSelectionManager._ballType[index].CostToUnlock)
					{
						gameManager.AddCoin(-ballSelectionManager._ballType[index].CostToUnlock);
						ballSelectionManager._ballType[index].isAvailable = true;
						ButtonText[index].text = _select;
					}
					
					else
					{
						
					}
				}
				break;
			}
			
			case 4:
			{
				//int index = 3;
				if(ballSelectionManager._ballType[index].isAvailable)
				{
					ballSelectionManager._ballType[index].isSelected = true;
					ButtonText[index].text = selected;
					for(int i =0 ;i<ballSelectionManager._ballType.Length;i++)
					{
						if(i != index)
						{
							ballSelectionManager._ballType[i].isSelected = false;
							if(ballSelectionManager._ballType[i].isAvailable)
								ButtonText[i].text = _select;
						}
					}
				}
				else
				{
					if(gameManager.GetCoins() >= ballSelectionManager._ballType[index].CostToUnlock)
					{
						gameManager.AddCoin(-ballSelectionManager._ballType[index].CostToUnlock);
						ballSelectionManager._ballType[index].isAvailable = true;
						ButtonText[index].text = _select;
					}
					
					else
					{
						
					}
				}
				break;
			}
			
			case 5:
			{
				//int index = 4;
				if(ballSelectionManager._ballType[index].isAvailable)
				{
					ballSelectionManager._ballType[index].isSelected = true;
					ButtonText[index].text = selected;
					for(int i =0 ;i<ballSelectionManager._ballType.Length;i++)
					{
						if(i != index)
						{
							ballSelectionManager._ballType[i].isSelected = false;
							if(ballSelectionManager._ballType[i].isAvailable)
								ButtonText[i].text = _select;
						}
					}
				}
				else
				{
					if(gameManager.GetCoins() >= ballSelectionManager._ballType[index].CostToUnlock)
					{
						gameManager.AddCoin(-ballSelectionManager._ballType[index].CostToUnlock);
						ballSelectionManager._ballType[index].isAvailable = true;
						ButtonText[index].text = _select;
					}
					
					else
					{
						
					}
				}
				break;
			}
			
		}
		
		// saving the ball selection state 
			for(int i=0;i<ballSelectionManager._ballType.Length;i++)
			{
				
				if(ballSelectionManager._ballType[i].isAvailable)
				{
					gameManager.UpdateIntDataInDevice("_ballTypeisAvailable"+i,1,false);
				}
				
				else
				{
					gameManager.UpdateIntDataInDevice("_ballTypeisAvailable" +i,0,false);
				}
				
				if(ballSelectionManager._ballType[i].isSelected)
				{
					gameManager.UpdateIntDataInDevice("_ballTypeisSelected"+i,1,false);
				}
				
				else
				{
					gameManager.UpdateIntDataInDevice("_ballTypeisSelected"+i,0,false);
					
				}
					
			}
		GuiTextManager.instance.UpdateText();
	}
	
	
	public void BuyBall(int index )
	{
		
		switch(index)
		{
			case 1:
			{
				
				if(gameManager.GetCoins() >= ballSelectionManager._ballType[index].CostToUnlock)
				{
					gameManager.AddCoin(-ballSelectionManager._ballType[index].CostToUnlock);
					ballSelectionManager.IncreaseBallAvailable(1);
					GuiTextManager.instance.UpdateText();
					gameManager.IncrementBallCountOfIndex(2);
//				    Debug.Log("Update Coins");
				}
			
			    else
				{
					CallCoinPurchagePage();
				}
				break;
			}
			
			case 2:
			{
				if(gameManager.GetCoins() >= ballSelectionManager._ballType[index].CostToUnlock)
				{
					gameManager.AddCoin(-ballSelectionManager._ballType[index].CostToUnlock);
					ballSelectionManager.IncreaseBallAvailable(2);
					GuiTextManager.instance.UpdateText();
					gameManager.IncrementBallCountOfIndex(3);
				}
			
				 else
				{
					CallCoinPurchagePage();
				}
				break;
			}
			
			case 3:
			{
				if(gameManager.GetCoins() >= ballSelectionManager._ballType[index].CostToUnlock)
				{
					gameManager.AddCoin(-ballSelectionManager._ballType[index].CostToUnlock);
					ballSelectionManager.IncreaseBallAvailable(3);
					GuiTextManager.instance.UpdateText();
					gameManager.IncrementBallCountOfIndex(4);
				}
			
				 else
				{
					CallCoinPurchagePage();
				}
				break;
			}
			
			case 4:
			{
				if(gameManager.GetCoins() >= ballSelectionManager._ballType[index].CostToUnlock)
				{
					gameManager.AddCoin(-ballSelectionManager._ballType[index].CostToUnlock);
				}
			
				 else
				{
					CallCoinPurchagePage();
				}
				break;
			}
			
			case 5:
			{
				break;
			}
		}
	}
	
	public void UpdateText()
	{
		NoOfballText[0].text = ballSelectionManager._ballType[1].noOfBallAvailable.ToString();
		NoOfballText[1].text = ballSelectionManager._ballType[2].noOfBallAvailable.ToString();
		NoOfballText[2].text = ballSelectionManager._ballType[3].noOfBallAvailable.ToString();
		ballUnlockingCost[0].text = ballSelectionManager._ballType[1].CostToUnlock.ToString();
		ballUnlockingCost[1].text = ballSelectionManager._ballType[2].CostToUnlock.ToString();
		ballUnlockingCost[2].text = ballSelectionManager._ballType[3].CostToUnlock.ToString();
		_noOfBallInpack[0].text = "AVAILABLE IN PACK OF"+ " "+ballSelectionManager._ballType[1].noOfBallInPak.ToString();
		_noOfBallInpack[1].text = "AVAILABLE IN PACK OF"+ " "+ballSelectionManager._ballType[2].noOfBallInPak.ToString();
		_noOfBallInpack[2].text = "AVAILABLE IN PACK OF"+ " "+ballSelectionManager._ballType[3].noOfBallInPak.ToString();
		_descriptionText[0].text = "SWING BALL";
		_descriptionText[1].text = "SPEEDY BALL";
		_descriptionText[2].text = "WIND RESISTIVE BALL";
	}
	
	public void CallCoinPurchagePage()
	{
		if(gameManager.isInappNotReq)
		{
			
			PopUpManager.instance.CreatePopUp(PopUpType.notenoughCoins);
			return;
		}
		# if UNITY_WEBPLAYER
					
		# else
			screenManager.LoadScreen("CoinPurchase");
			subscreenitemmanager.LoadScreen("Empty");
			screenManager.prevPageBeforeCoinPurchage = "ShopScreen";
		#endif
	}
}
