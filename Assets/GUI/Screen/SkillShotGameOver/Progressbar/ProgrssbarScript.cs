using UnityEngine;
using System.Collections;

public class ProgrssbarScript : MonoBehaviour 
{
	private GameManager _gameManager;
	private ScoreManager _scoreManager;
	public enum ProgressBarType
	{
	    Horizontal,
	    Vertical,
	}
    public Transform _AnchorObjectTransform;
    public ProgressBarType _Type;
    public float _Completion = 0.0f;

    private Vector2 mvTextureScale = new Vector2(1.0f,1.0f);
    private Vector2 mvTextureOffset = new Vector2(0.0f,0.0f);
    private Vector3 mvAnchorScale = new Vector3(1.0f,1.0f,1.0f);
    public GameObject _barParticle;
	private GameObject _barParticleClone;
	float XcmpparticleStartPos = -0.295f;
	float XcmpparticleFinalPos = 0.295f;
	Vector3 currentParticlePos;
	public float _dummyCompletion;
	int waitingTime;
	float startTime;
	bool isCounter = false;
	int time ;
	// Use this for initialization
    void Start () 
    {
		_gameManager = GameObject.Find("GameManager").GetComponent("GameManager") as GameManager ;
		_scoreManager = GameObject.Find("GameManager").GetComponent("ScoreManager") as ScoreManager ;
		_Completion =  _gameManager.UpdateFloatDataInDevice("Completion",_Completion,true);
		_dummyCompletion = 	_scoreManager.GetLevelUpXp()/((float)_gameManager.GetLevelUpCutoff());
        if(_AnchorObjectTransform == null)
            _AnchorObjectTransform = gameObject.transform;
		_barParticleClone = Instantiate(_barParticle) as GameObject;
		currentParticlePos = _barParticleClone.transform.position;
		currentParticlePos.x = (((XcmpparticleFinalPos-XcmpparticleStartPos)*(_Completion-0.0f))/(1.0f-0.0f)) + XcmpparticleStartPos;
		_barParticleClone.transform.position = currentParticlePos;
		
    }
	
	void Update()
	{
		if(isCounter)
		{
			
			time = (int)(Time.time-startTime);
			
			if(time> waitingTime)
			{
				_gameManager.CallGameOverScreen();
				isCounter = false;
			}
		}
	}

    public void setCompletion(float completion)
    {
        switch(_Type)
        {
            case ProgressBarType.Horizontal:
                mvTextureScale.x = completion;
                mvAnchorScale.x = completion;
                mvTextureOffset.x = 1.0f-completion;
                _AnchorObjectTransform.localScale = mvAnchorScale;
                GetComponent<Renderer>().material.mainTextureScale = mvTextureScale;
                GetComponent<Renderer>().material.mainTextureOffset = mvTextureOffset;
                break;
            case ProgressBarType.Vertical:
                mvTextureScale.y = completion;
                mvAnchorScale.y = completion;
                mvTextureOffset.y = 0;
                _AnchorObjectTransform.localScale = mvAnchorScale;
                GetComponent<Renderer>().material.mainTextureScale = mvTextureScale;
                GetComponent<Renderer>().material.mainTextureOffset = mvTextureOffset;
                break;
            default:
                break;
        }
    }

    void FixedUpdate ()
    {
		setCompletion(_Completion);
		_Completion = Mathf.MoveTowards(_Completion,_dummyCompletion,0.002f);
		if(_barParticleClone!=null)
		{
			currentParticlePos.x = (((XcmpparticleFinalPos-XcmpparticleStartPos)*(_Completion-0.0f))/(1.0f-0.0f)) + XcmpparticleStartPos;
			_barParticleClone.transform.position = currentParticlePos;
		}
		if(_Completion == _dummyCompletion)
		{
			
			
			if(_gameManager.ExtraLveluPPoint()>0)
			{
				
				 _scoreManager.SetLevelUpXp(_gameManager.ExtraLveluPPoint());
				_dummyCompletion = _scoreManager.GetLevelUpXp()/((float)_gameManager.GetLevelUpCutoff());
				print (_dummyCompletion);
				_Completion = 0;
				print (_Completion);
				_gameManager.SetExtraLveluPPoint(0);
				
			}
		
			else
			{
				_gameManager.UpdateFloatDataInDevice("Completion",_Completion,false);	
				Destroy (_barParticleClone);
				if(!isCounter)
			  	Delay(1);
			}
		}
    }
	
	
	public void Delay(int time)
	{
		waitingTime = time;
		startTime = Time.time;
		isCounter = true;
	}
	
}