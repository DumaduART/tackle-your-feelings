using UnityEngine;
using System.Collections;

public class PurchaseScreenGUIItemsManager : GUIItemsManager
{	
	public static PurchaseScreenGUIItemsManager instance;
	ScreenManager subscreenitemmanager;	
	public GameObject ShopSubScreenManager;
	GameObject tempsubscreenmanager;
	GameManager _gamemanager;
	public GUIText _removeAdsText;
	public GUIText _totalCoinsText;
	public bool isTrue ;
	
	bool _isAndroidBack = false;
	public GUIText[] purchaseValues;
	string[] ids;
	void Awake()
	{
		instance = this;
		ids = new string[]{"1","1","1","1","1","1"};
	#if UNITY_STANDALONE_OSX
			// disable current Screen button
		_removeAdsText.gameObject.SetActive(false);
		CheckButton(false);
		
		#endif
			
	}
	
	void Start ()
	{
		base.Init();
		_gamemanager = GameObject.Find("GameManager").GetComponent<GameManager>();
		
		#if !UNITY_STANDALONE_OSX
			if(_gamemanager.checkingAdd)
			{
				_removeAdsText.text = "REMOVE ADS WITH ANY PURCHASE";
			}
			else
			{
				_removeAdsText.text = "Checkout for more games from Game++";
			}
		#endif
		GUIManager.instance.Bgtexture.gameObject.SetActive(true);
	}	
	
	public void OnSubScreenSelectedEvent(GUIItem item)
	{
		//Debug.Log(item.name + "here");
		
		if(meState != eGUI_ITEMS_MANAGER_STATE.Settled)
			return;		
	}
	
	void Update()
	{
		#if !UNITY_STANDALONE_OSX
				if(_removeAdsText.color.a.Equals(1))
				{
					isTrue = true;
				}
				else if(_removeAdsText.color.a.Equals(0))
				{
					isTrue = false;
				}
				
				if(isTrue)
				{
					Color c = _removeAdsText.color;
					c.a = Mathf.MoveTowards(c.a , 0 , 2 * Time.deltaTime);
					_removeAdsText.color = c;
			
				}
				else
				{
					Color c = _removeAdsText.color;
					c.a = Mathf.MoveTowards(c.a , 1 , 3 * Time.deltaTime);
					_removeAdsText.color = c;
					
				}
		#endif
				if(Input.GetKeyDown(KeyCode.Escape))
				{
					if(_isAndroidBack)
					{
						_screenManager.LoadScreen(_screenManager.prevPageBeforeCoinPurchage);
						_isAndroidBack = false;
					}
				}
			
	}
	
	public override void OnEntryAnimationCompleted()
	{
		_isAndroidBack = true;
	}
	
	public override void OnSelectedEvent(GUIItem item)
	{		
		if(meState == eGUI_ITEMS_MANAGER_STATE.Settled && _screenManager != null)
		{
			
			if(item.name == "BackBG" )
			{
					_screenManager.LoadScreen(_screenManager.prevPageBeforeCoinPurchage);
				  //  subscreenitemmanager.LoadScreen("Empty"); //Empty
			}
			if(item.name == "BarBack0")
			{
				
			}
			if(item.name == "BarBack1")
			{
				#if UNITY_STANDALONE_OSX
				CheckButton(false);
				#endif
				_gamemanager.InAppCalling(1);
			}
			
			if(item.name == "BarBack2")
			{
				#if UNITY_STANDALONE_OSX
				CheckButton(false);
				#endif
				_gamemanager.InAppCalling(2);
			}
			
			if(item.name == "BarBack3")
			{
				#if UNITY_STANDALONE_OSX
				CheckButton(false);
				#endif
				_gamemanager.InAppCalling(3);
			}
			
			if(item.name == "BarBack4")
			{
				#if UNITY_STANDALONE_OSX
				CheckButton(false);
				#endif
				_gamemanager.InAppCalling(4);
			}
			
			if(item.name == "BarBack5")
			{
				#if UNITY_STANDALONE_OSX
				CheckButton(false);
				#endif
				_gamemanager.InAppCalling(5);
			}
			
			if(item.name == "BarBack6")
			{
				#if UNITY_STANDALONE_OSX
				CheckButton(false);
				#endif
				_gamemanager.InAppCalling(6);
			}
			
			if(item.name == "ResetButton")
			{
				PlayerPrefs.DeleteAll();
			}
			
			
		}
	}	
	void ShowVideoAds()
	{
				//Debug.Log("video..");
				GameManager.instance.AddCoin (50);
				GuiTextManager.instance.UpdateText();

	}
	void GetInAppData()
	{
				for(int i = 0 ; i<purchaseValues.Length;i++)
				{
					purchaseValues[i].text = PlayerPrefs.GetString(ids[i]);
				}
	}
	
	
	public void CheckButton(bool status)
	{
		
		GUIItemButton[] buttons = transform.GetComponentsInChildren<GUIItemButton>();		
		foreach(GUIItemButton button in buttons)
		{
			button.enabled = status;
		}
	}

}