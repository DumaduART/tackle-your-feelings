using UnityEngine;
using System.Collections;

public class HordingboardMovment : MonoBehaviour {

	// Use this for initialization
	public enum MovementType
	 {
		right=0,left
	 };
	private MovementType movementType;
	 public bool _isRotation;
	 public bool _ismovement;
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(_isRotation)
		{
			transform.Rotate(Vector3.forward * Time.deltaTime*40);
		}
		if(_ismovement)
		{
			switch(movementType)
			{
				case MovementType.right:
				{
					transform.Translate(Vector3.right * Time.deltaTime*2);
					break;
				}
				case MovementType.left:
				{
					transform.Translate(-Vector3.right * Time.deltaTime*2);
					break;
				}
			}
		
		}
		if(transform.position.x>=7)
			movementType=MovementType.left;
		if(transform.position.x<=-15)
			movementType=MovementType.right;
		
	}
}
